<?xml version="1.0" encoding="utf-8"?>
<layout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    tools:context=".presentation.book_details.BookDetailsFragment">

    <data>

        <variable
            name="item"
            type="com.example.mybooks.domain.model.Book" />
    </data>

    <androidx.constraintlayout.widget.ConstraintLayout
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:padding="4dp">

        <ImageView
            android:id="@+id/imageBook"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:layout_margin="16dp"
            android:contentDescription="@string/image_for_book_detail"
            android:src="@drawable/book_open"
            app:layout_constraintTop_toTopOf="parent" />


        <TextView
            android:id="@+id/titleTextView"
            android:layout_width="0dp"
            android:layout_height="wrap_content"
            android:layout_marginHorizontal="8dp"
            android:layout_marginStart="8dp"
            android:layout_marginTop="32dp"
            android:text="@{item.title}"
            android:textColor="@android:color/black"
            android:textSize="18sp"
            android:textStyle="bold"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toBottomOf="@id/imageBook"
            tools:text="flexibility dynamic Cove" />

        <TextView
            android:id="@+id/authorLabelTextView"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:layout_marginTop="12dp"
            android:text="@string/author"
            android:textStyle="bold"
            app:layout_constraintStart_toStartOf="@id/titleTextView"
            app:layout_constraintTop_toBottomOf="@id/titleTextView" />

        <TextView
            android:id="@+id/authorTextView"
            android:layout_width="0dp"
            android:layout_height="wrap_content"
            android:layout_marginHorizontal="8dp"
            android:text="@{item.author}"
            android:textSize="16sp"
            app:layout_constraintBaseline_toBaselineOf="@id/authorLabelTextView"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintStart_toEndOf="@id/authorLabelTextView"
            tools:text="Gabriel García Márquez" />

        <TextView
            android:id="@+id/genreLabelTextView"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:layout_marginTop="12dp"
            android:text="@string/genre"
            android:textStyle="bold"
            app:layout_constraintStart_toStartOf="@id/authorLabelTextView"
            app:layout_constraintTop_toBottomOf="@id/authorLabelTextView" />

        <TextView
            android:id="@+id/genreTextView"
            android:layout_width="0dp"
            android:layout_height="wrap_content"
            android:layout_marginHorizontal="8dp"
            android:text="@{item.genre}"
            android:textSize="16sp"
            app:layout_constraintBaseline_toBaselineOf="@id/genreLabelTextView"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintStart_toEndOf="@id/genreLabelTextView"
            tools:text="fiction" />

        <TextView
            android:id="@+id/yearPublishedLabelTextView"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:layout_marginTop="12dp"
            android:text="@string/year_published"
            android:textStyle="bold"
            app:layout_constraintStart_toStartOf="@id/genreLabelTextView"
            app:layout_constraintTop_toBottomOf="@id/genreLabelTextView" />


        <TextView
            android:id="@+id/yearPublishedTextView"
            android:layout_width="0dp"
            android:layout_height="wrap_content"
            android:layout_marginHorizontal="8dp"
            android:text="@{item.yearPublished}"
            android:textSize="16sp"
            app:layout_constraintBaseline_toBaselineOf="@id/yearPublishedLabelTextView"
            app:layout_constraintEnd_toStartOf="@id/deleteBookButton"
            app:layout_constraintStart_toEndOf="@id/yearPublishedLabelTextView"
            tools:text="1967" />

        <TextView
            android:id="@+id/createdAtLabelTextView"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:layout_marginBottom="12dp"
            android:text="@string/created_at"
            android:textStyle="bold"
            app:layout_constraintBottom_toTopOf="@id/checkInCheckOutButton"
            app:layout_constraintEnd_toStartOf="@+id/createdAtTextView"
            app:layout_constraintHorizontal_chainStyle="packed"
            app:layout_constraintStart_toStartOf="parent" />


        <TextView
            android:id="@+id/createdAtTextView"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:layout_marginStart="8dp"
            android:text="@{item.createdAt}"
            android:textSize="16sp"
            app:layout_constraintBottom_toBottomOf="@id/createdAtLabelTextView"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintStart_toEndOf="@+id/createdAtLabelTextView"
            tools:text="2023-12-29 11:02:41" />

        <ImageView
            android:id="@+id/deleteBookButton"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:layout_margin="12dp"
            android:contentDescription="@string/delete_book"
            android:scaleX="1.5"
            android:scaleY="1.5"
            android:src="@drawable/ic_delete"
            app:layout_constraintBottom_toBottomOf="parent"
            app:layout_constraintEnd_toEndOf="parent" />

        <Button
            android:id="@+id/checkInCheckOutButton"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:layout_margin="4dp"
            android:background="@color/colorPrimaryDark"
            android:paddingHorizontal="18dp"
            android:text="@{item.checkedOut ? @string/check_in : @string/check_out}"
            android:textColor="@color/white"
            app:layout_constraintBottom_toBottomOf="parent"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintStart_toStartOf="parent"
            tools:text="@string/check_out" />

        <androidx.constraintlayout.widget.Group
            android:id="@+id/loading"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:visibility="gone"
            app:constraint_referenced_ids="progressBar,loadingBgView" />

        <View
            android:id="@+id/loadingBgView"
            android:layout_width="match_parent"
            android:layout_height="match_parent"
            android:alpha="0.3"
            android:background="@android:color/black" />

        <ProgressBar
            android:id="@+id/progressBar"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:progressTint="@color/colorPrimaryDark"
            app:layout_constraintBottom_toBottomOf="parent"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toTopOf="parent" />
    </androidx.constraintlayout.widget.ConstraintLayout>
</layout>