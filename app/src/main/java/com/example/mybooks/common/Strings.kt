package com.example.mybooks.common

import androidx.annotation.StringRes
import com.example.mybooks.BooksApp

object Strings {
    fun get(@StringRes stringRes: Int, vararg formatArgs: Any = emptyArray()) =
        BooksApp.instance.getString(stringRes, *formatArgs)
}