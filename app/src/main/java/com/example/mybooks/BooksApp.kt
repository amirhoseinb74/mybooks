package com.example.mybooks

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class BooksApp : Application() {
    companion object {
        lateinit var instance: BooksApp private set
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
    }
}