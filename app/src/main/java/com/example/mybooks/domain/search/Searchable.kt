package com.example.mybooks.domain.search

interface Searchable {
    fun doesMatchSearchQuery(query: String): Boolean
}