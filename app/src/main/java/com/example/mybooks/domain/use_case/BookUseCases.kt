package com.example.mybooks.domain.use_case

data class BookUseCases(
    val getBooks: GetBooks,
    val getBook: GetBook,
    val addBook: AddBook,
    val updateBook: UpdateBook,
    val deleteBook: DeleteBook
)
