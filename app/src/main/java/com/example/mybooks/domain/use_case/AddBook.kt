package com.example.mybooks.domain.use_case

import com.example.mybooks.common.Resource
import com.example.mybooks.data.remote.dto.AddBookDto
import com.example.mybooks.data.remote.dto.ApiResponse
import com.example.mybooks.domain.exception.InvalidBookException
import com.example.mybooks.domain.exception.mapNetworkExceptionToMessage
import com.example.mybooks.domain.repository.BooksRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flow

class AddBook(
    private val repository: BooksRepository
) {
    operator fun invoke(
        title: String,
        author: String,
        genre: String,
        yearPublished: String
    ): Flow<Resource<ApiResponse>> = flow {
        if (title.isEmpty()) {
            throw InvalidBookException("Title can't be empty")
        }
        if (author.isEmpty()) {
            throw InvalidBookException("Author can't be empty")
        }
        if (genre.isEmpty()) {
            throw InvalidBookException("Genre can't be empty")
        }
        if (yearPublished.isEmpty()) {
            throw InvalidBookException("Year can't be empty")
        }
        val addBookDto = AddBookDto(
            title = title,
            author = author,
            genre = genre,
            yearPublished = yearPublished.toInt()
        )
        emit(Resource.Loading())
        val apiResponse = repository.addBook(addBookDto)
        emit(Resource.Success(apiResponse))
    }.catch { e ->
        emit(Resource.Error(e.mapNetworkExceptionToMessage()))
    }
}