package com.example.mybooks.domain.model

import com.example.mybooks.data.remote.dto.BookDto
import com.example.mybooks.domain.search.Searchable

data class Book(
    val id: String,
    val title: String,
    val author: String,
    val checkedOut: Boolean,
    val createdAt: String,
    val genre: String,
    val yearPublished: String
) : Searchable {
    override fun doesMatchSearchQuery(query: String): Boolean {
        val matchCombinations = listOf(title, author, genre, yearPublished)
        return matchCombinations.any {
            it.contains(query, ignoreCase = true)
        }
    }
}

fun BookDto.toBook() = Book(
    id = id ?: "",
    title = title ?: "",
    author = author ?: "",
    checkedOut = checkedOut ?: false,
    createdAt = createdAt ?: "",
    genre = genre ?: "",
    yearPublished = yearPublished.toString(),
)