package com.example.mybooks.domain.use_case

import com.example.mybooks.common.Resource
import com.example.mybooks.data.remote.dto.ApiResponse
import com.example.mybooks.data.remote.dto.UpdateBookDto
import com.example.mybooks.domain.exception.mapNetworkExceptionToMessage
import com.example.mybooks.domain.repository.BooksRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flow

class UpdateBook(
    private val repository: BooksRepository
) {
    operator fun invoke(id: String, checkedOut: Boolean): Flow<Resource<ApiResponse>> = flow {
        val updateBookDto = UpdateBookDto(checkedOut = checkedOut)
        emit(Resource.Loading())
        val apiResponse = repository.updateBook(id, updateBookDto)
        emit(Resource.Success(apiResponse))
    }.catch { e ->
        emit(Resource.Error(e.mapNetworkExceptionToMessage()))
    }
}