package com.example.mybooks.domain.exception

import com.example.mybooks.R
import com.example.mybooks.common.Strings
import retrofit2.HttpException
import java.io.IOException
import java.net.SocketTimeoutException

fun Throwable.mapNetworkExceptionToMessage(): String {
    return when (this) {
        is HttpException -> localizedMessage ?: Strings.get(R.string.unexpected_error)
        is SocketTimeoutException, is IOException -> Strings.get(R.string.check_internet_connection)
        is NoDataException -> message ?: Strings.get(R.string.there_is_no_data)
        is InvalidBookException -> message ?: Strings.get(R.string.something_went_wrong)
        else -> Strings.get(R.string.something_went_wrong)
    }
}