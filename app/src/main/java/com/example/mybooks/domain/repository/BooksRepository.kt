package com.example.mybooks.domain.repository

import com.example.mybooks.data.remote.dto.AddBookDto
import com.example.mybooks.data.remote.dto.ApiResponse
import com.example.mybooks.data.remote.dto.BookDto
import com.example.mybooks.data.remote.dto.UpdateBookDto

interface BooksRepository {

    suspend fun getBooks(): List<BookDto>

    suspend fun getBook(id: String): BookDto

    suspend fun addBook(addBookDto: AddBookDto): ApiResponse

    suspend fun updateBook(id: String, updateBookDto: UpdateBookDto): ApiResponse

    suspend fun deleteBook(id: String): ApiResponse
}