package com.example.mybooks.domain.use_case

import com.example.mybooks.common.Resource
import com.example.mybooks.domain.exception.mapNetworkExceptionToMessage
import com.example.mybooks.domain.model.Book
import com.example.mybooks.domain.model.toBook
import com.example.mybooks.domain.repository.BooksRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flow

class GetBook(
    private val repository: BooksRepository
) {
    operator fun invoke(id: String): Flow<Resource<Book>> = flow {
        emit(Resource.Loading())
        val book: Book = repository.getBook(id).toBook()
        emit(Resource.Success(book))
    }.catch { e ->
        emit(Resource.Error(e.mapNetworkExceptionToMessage()))
    }
}