package com.example.mybooks.domain.exception

class InvalidBookException(message: String) : Exception(message)