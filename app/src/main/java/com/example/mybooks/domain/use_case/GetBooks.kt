package com.example.mybooks.domain.use_case

import com.example.mybooks.common.Resource
import com.example.mybooks.domain.exception.NoDataException
import com.example.mybooks.domain.exception.mapNetworkExceptionToMessage
import com.example.mybooks.domain.model.Book
import com.example.mybooks.domain.model.toBook
import com.example.mybooks.domain.repository.BooksRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flow

/**
 * Use the [GetBooks] class to retrieve a list of books from the provided [repository].
 *
 * In testing scenarios, you can annotate the [repository] parameter with
 * `@FakeRepository` to obtain test data.
 * @param repository The repository responsible for providing book data.
 */
class GetBooks(
    private val repository: BooksRepository
) {
    operator fun invoke(): Flow<Resource<List<Book>>> = flow {
        emit(Resource.Loading())
        emit(Resource.Success(repository.getBooks().map { it.toBook() }.takeIf { it.isNotEmpty() }
            ?: throw NoDataException()))
    }.catch { e ->
        emit(Resource.Error(e.mapNetworkExceptionToMessage()))
    }
}