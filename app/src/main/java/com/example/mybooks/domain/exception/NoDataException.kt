package com.example.mybooks.domain.exception

import com.example.mybooks.R
import com.example.mybooks.common.Strings

class NoDataException : Exception(Strings.get(R.string.there_is_no_data))