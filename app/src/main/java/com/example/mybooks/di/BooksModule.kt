package com.example.mybooks.di

import com.example.mybooks.data.remote.BooksApi
import com.example.mybooks.data.repository.BooksRepositoryImpl
import com.example.mybooks.data.repository.fake.FakeBooksRepository
import com.example.mybooks.domain.repository.BooksRepository
import com.example.mybooks.domain.use_case.AddBook
import com.example.mybooks.domain.use_case.BookUseCases
import com.example.mybooks.domain.use_case.DeleteBook
import com.example.mybooks.domain.use_case.GetBook
import com.example.mybooks.domain.use_case.GetBooks
import com.example.mybooks.domain.use_case.UpdateBook
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import javax.inject.Qualifier
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object BooksModule {

    /**
     * @param retrofit provided in NetworkModule in this package
     * */
    @Provides
    @Singleton
    fun provideBooksApi(retrofit: Retrofit): BooksApi = retrofit.create(BooksApi::class.java)

    @Provides
    @Singleton
    fun provideBooksRepository(booksApi: BooksApi): BooksRepository = BooksRepositoryImpl(booksApi)

    @FakeRepository
    @Provides
    @Singleton
    fun provideFakeBooksRepository(): BooksRepository = FakeBooksRepository()

    @Provides
    @Singleton
    fun provideBookUseCases(repository: BooksRepository): BookUseCases = BookUseCases(
        getBooks = GetBooks(repository),
        getBook = GetBook(repository),
        addBook = AddBook(repository),
        updateBook = UpdateBook(repository),
        deleteBook = DeleteBook(repository)
    )
}

@Retention(AnnotationRetention.RUNTIME)
@Qualifier
annotation class FakeRepository