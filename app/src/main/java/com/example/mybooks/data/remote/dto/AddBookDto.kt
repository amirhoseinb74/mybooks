package com.example.mybooks.data.remote.dto

data class AddBookDto(
    val title: String,
    val author: String,
    val genre: String,
    val yearPublished: Int
)