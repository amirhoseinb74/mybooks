package com.example.mybooks.data.remote.dto

data class UpdateBookDto(
    val checkedOut: Boolean
)