package com.example.mybooks.data.remote.dto

data class ApiResponse(
    val message: String,
    val status: String? = null
)
