package com.example.mybooks.data.remote

import com.example.mybooks.data.remote.dto.AddBookDto
import com.example.mybooks.data.remote.dto.ApiResponse
import com.example.mybooks.data.remote.dto.BookDto
import com.example.mybooks.data.remote.dto.UpdateBookDto
import retrofit2.http.Body
import retrofit2.http.DELETE
import retrofit2.http.GET
import retrofit2.http.PATCH
import retrofit2.http.POST
import retrofit2.http.Path


interface BooksApi {

    @GET("books")
    suspend fun getBooks(): List<BookDto>

    @GET("books/{id}")
    suspend fun getBook(@Path("id") id: String): BookDto

    @POST("books")
    suspend fun addBook(@Body addBookDto: AddBookDto): ApiResponse

    @PATCH("books/{id}")
    suspend fun updateBook(@Path("id") id: String, @Body updateBookDto: UpdateBookDto): ApiResponse

    @DELETE("books/{id}")
    suspend fun deleteBook(@Path("id") id: String): ApiResponse
}