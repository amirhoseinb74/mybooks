package com.example.mybooks.data.remote.dto

data class BookDto(
    val id: String?,
    val title: String?,
    val author: String?,
    val checkedOut: Boolean?,
    val createdAt: String?,
    val genre: String?,
    val yearPublished: Int?,
)