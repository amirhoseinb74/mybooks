package com.example.mybooks.data.repository

import com.example.mybooks.data.remote.BooksApi
import com.example.mybooks.data.remote.dto.AddBookDto
import com.example.mybooks.data.remote.dto.ApiResponse
import com.example.mybooks.data.remote.dto.BookDto
import com.example.mybooks.data.remote.dto.UpdateBookDto
import com.example.mybooks.domain.repository.BooksRepository

class BooksRepositoryImpl(
    private val api: BooksApi
) : BooksRepository {

    override suspend fun getBooks(): List<BookDto> = api.getBooks()

    override suspend fun getBook(id: String): BookDto = api.getBook(id)

    override suspend fun addBook(addBookDto: AddBookDto): ApiResponse = api.addBook(addBookDto)


    override suspend fun updateBook(id: String, updateBookDto: UpdateBookDto): ApiResponse =
        api.updateBook(id, updateBookDto)


    override suspend fun deleteBook(id: String): ApiResponse = api.deleteBook(id)

}