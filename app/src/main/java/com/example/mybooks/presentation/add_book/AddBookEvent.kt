package com.example.mybooks.presentation.add_book

sealed class AddBookEvent {
    data class ShowMessage(val message: String?) : AddBookEvent()
    object NavigateBack : AddBookEvent()
}