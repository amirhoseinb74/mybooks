package com.example.mybooks.presentation.splash

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import com.example.mybooks.R
import com.example.mybooks.presentation.ui.spacing
import com.example.mybooks.presentation.ui.theme.MyBooksTheme
import kotlinx.coroutines.delay

@Composable
fun SplashScreen(navigateToHome: () -> Unit) {
    Box(
        modifier = Modifier
            .fillMaxSize()
            .background(Color.White)
    ) {
        Image(
            painter = painterResource(id = R.drawable.book_shelf),
            contentDescription = "Bookshelf Image",
            modifier = Modifier
                .align(Alignment.Center)
                .padding(horizontal = MaterialTheme.spacing.xLarge)
        )
        Text(
            modifier = Modifier
                .padding(bottom = MaterialTheme.spacing.large)
                .align(Alignment.BottomCenter),
            text = stringResource(id = R.string.app_name),
            style = MaterialTheme.typography.headlineLarge
        )
    }

    LaunchedEffect(key1 = true) {
        delay(2000)
        navigateToHome()
    }
}

@Preview
@Composable
fun SplashScreenPreview() {
    MyBooksTheme {
        SplashScreen(navigateToHome = {})
    }
}