package com.example.mybooks.presentation.add_book

import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.example.mybooks.common.Resource
import com.example.mybooks.domain.use_case.BookUseCases
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Job
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class AddBookViewModel @Inject constructor(
    private val bookUseCases: BookUseCases
) : ViewModel() {

    private var addBookJob: Job? = null

    private val _isLoading = MutableStateFlow(false)
    val isLoading = _isLoading.asStateFlow().asLiveData()

    private val addBookEventsChannel = Channel<AddBookEvent>()
    val addBookEvent = addBookEventsChannel.receiveAsFlow()

    fun addBook(
        title: String,
        author: String,
        genre: String,
        yearPublished: String
    ) {
        addBookJob?.cancel()
        addBookJob = bookUseCases.addBook(title, author, genre, yearPublished).onEach { result ->
            when (result) {
                is Resource.Loading -> {
                    _isLoading.update { true }
                }

                is Resource.Success -> {
                    _isLoading.update { false }
                    navigateBack()
                }

                is Resource.Error -> {
                    _isLoading.update { false }
                    showErrorMessage(result.message)
                }
            }
        }.launchIn(viewModelScope)
    }

    private fun showErrorMessage(message: String?) = viewModelScope.launch {
        addBookEventsChannel.send(AddBookEvent.ShowMessage(message))
    }

    private fun navigateBack() = viewModelScope.launch {
        addBookEventsChannel.send(AddBookEvent.NavigateBack)
    }
}