package com.example.mybooks.presentation.util

import androidx.appcompat.widget.SearchView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.example.mybooks.R
import com.google.android.material.snackbar.Snackbar

inline fun SearchView.onQueryTextChanged(crossinline listener: (String) -> Unit) {
    this.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
        override fun onQueryTextSubmit(query: String?): Boolean {
            return true
        }

        override fun onQueryTextChange(newText: String?): Boolean {
            listener(newText.orEmpty())
            return true
        }
    })
}

fun Fragment.showSnackbar(message: String?) {
    Snackbar.make(
        requireView(), message ?: getString(R.string.something_went_wrong), Snackbar.LENGTH_LONG
    ).run {
        setBackgroundTint(ContextCompat.getColor(requireContext(), R.color.red))
        setTextColor(ContextCompat.getColor(requireContext(), R.color.white))
        show()
    }
}