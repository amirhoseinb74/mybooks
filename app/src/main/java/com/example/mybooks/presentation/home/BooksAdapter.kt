package com.example.mybooks.presentation.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.mybooks.databinding.ItemBookBinding
import com.example.mybooks.domain.model.Book

class BooksAdapter(private val listener: BooksClickListener) :
    ListAdapter<Book, BooksAdapter.BooksViewHolder>(DiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BooksViewHolder {
        ItemBookBinding.inflate(LayoutInflater.from(parent.context), parent, false).also {
            return BooksViewHolder(it)
        }
    }

    override fun onBindViewHolder(holder: BooksViewHolder, position: Int) {
        holder.onBind(position)
    }

    inner class BooksViewHolder(private val binding: ItemBookBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun onBind(position: Int) {
            val currentBook = getItem(position)
            binding.apply {
                item = currentBook
                clRoot.setOnClickListener {
                    listener.onItemClick(currentBook.id)
                }
                checkInCheckOutButton.setOnClickListener {
                    listener.onCheckInCheckOutClick(currentBook.id, !currentBook.checkedOut)
                }
                deleteBookButton.setOnClickListener {
                    listener.onDeleteClick(currentBook.id)
                }
            }
        }
    }

    interface BooksClickListener {
        fun onItemClick(id: String)
        fun onDeleteClick(id: String)
        fun onCheckInCheckOutClick(id: String, checkOut: Boolean)
    }

    class DiffCallback : DiffUtil.ItemCallback<Book>() {
        override fun areItemsTheSame(oldItem: Book, newItem: Book): Boolean =
            oldItem.id == newItem.id

        override fun areContentsTheSame(oldItem: Book, newItem: Book): Boolean =
            oldItem == newItem
    }
}