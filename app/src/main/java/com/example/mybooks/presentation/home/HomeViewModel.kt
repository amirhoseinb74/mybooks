package com.example.mybooks.presentation.home


import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.example.mybooks.common.Resource
import com.example.mybooks.domain.model.Book
import com.example.mybooks.domain.use_case.BookUseCases
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.Job
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.debounce
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

@OptIn(FlowPreview::class)
@HiltViewModel
class HomeViewModel @Inject constructor(
    private val bookUseCases: BookUseCases
) : ViewModel() {

    private val homeEventChannel = Channel<HomeEvent>()
    val homeEvent = homeEventChannel.receiveAsFlow()

    private val _searchText = MutableStateFlow("")
    val searchText = _searchText.asStateFlow()

    private val _isLoading = MutableStateFlow(false)
    val isLoading = _isLoading.asStateFlow().asLiveData()

    private val _books = MutableStateFlow<List<Book>>(emptyList())
    val books = searchText
        //some delay to let user finish typing
        .debounce(500L)
        .combine(_books) { text, books ->
            if (text.isBlank()) {
                books
            } else {
                books.filter { it.doesMatchSearchQuery(text) }
            }
        }
        .asLiveData()

    fun onSearchTextChange(text: String) {
        _searchText.value = text
    }

    private var getBooksJob: Job? = null
    private var deleteBookJob: Job? = null
    private var updateBookJob: Job? = null

    init {
        getBooks()
    }

    fun getBooks() {
        getBooksJob?.cancel()
        getBooksJob = bookUseCases.getBooks().onEach { result ->
            when (result) {
                is Resource.Loading -> {
                    _isLoading.update { true }
                }

                is Resource.Success -> {
                    _isLoading.update { false }
                    result.data?.let { books ->
                        _books.update { books }
                    }
                }

                is Resource.Error -> {
                    _isLoading.update { false }
                    showErrorMessage(result.message)
                }
            }
        }.launchIn(viewModelScope)
    }

    fun deleteBook(id: String) {
        deleteBookJob?.cancel()
        deleteBookJob = bookUseCases.deleteBook(id).onEach { result ->
            when (result) {
                is Resource.Loading -> {
                    _isLoading.update { true }
                }

                is Resource.Success -> {
                    _isLoading.update { false }
                    getBooks()
                }

                is Resource.Error -> {
                    _isLoading.update { false }
                    showErrorMessage(result.message)
                }
            }
        }.launchIn(viewModelScope)
    }


    fun updateBook(id: String, checkedOut: Boolean) {
        updateBookJob?.cancel()
        updateBookJob = bookUseCases.updateBook(id, checkedOut).onEach { result ->
            when (result) {
                is Resource.Loading -> {
                    _isLoading.update { true }
                }

                is Resource.Success -> {
                    _isLoading.update { false }
                    getBooks()
                }

                is Resource.Error -> {
                    _isLoading.update { false }
                    showErrorMessage(result.message)
                }
            }
        }.launchIn(viewModelScope)
    }

    private fun showErrorMessage(message: String?) = viewModelScope.launch {
        homeEventChannel.send(HomeEvent.ShowMessage(message))
    }

    override fun onCleared() {
        getBooksJob?.cancel()
        deleteBookJob?.cancel()
        updateBookJob?.cancel()
        super.onCleared()
    }
}
