package com.example.mybooks.presentation.book_details

sealed class BookDetailsEvent {
    data class ShowMessage(val message: String?) : BookDetailsEvent()
    object NavigateBack : BookDetailsEvent()
}