package com.example.mybooks.presentation.book_details

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.example.mybooks.common.Resource
import com.example.mybooks.domain.model.Book
import com.example.mybooks.domain.use_case.BookUseCases
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Job
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class BookDetailsViewModel @Inject constructor(
    private val bookUseCases: BookUseCases,
    savedStateHandle: SavedStateHandle
) : ViewModel() {

    private var getBookDetailJob: Job? = null
    private var deleteBookJob: Job? = null
    private var updateBookJob: Job? = null

    private val _isLoading = MutableStateFlow(false)
    val isLoading = _isLoading.asStateFlow().asLiveData()

    private val _book = MutableStateFlow<Book?>(null)
    val book = _book.asStateFlow().asLiveData()

    private val bookDetailsEventsChannel = Channel<BookDetailsEvent>()
    val bookDetailEvent = bookDetailsEventsChannel.receiveAsFlow()

    init {
        savedStateHandle.get<String>(ARG_BOOK_ID)?.let { id ->
            getBookDetails(id)
        }
    }

    private fun getBookDetails(id: String) {
        getBookDetailJob?.cancel()
        getBookDetailJob = bookUseCases.getBook(id).onEach { result ->
            when (result) {
                is Resource.Loading -> {
                    _isLoading.update { true }
                }

                is Resource.Success -> {
                    _isLoading.update { false }
                    result.data?.let { book ->
                        _book.update { book }
                    }
                }

                is Resource.Error -> {
                    _isLoading.update { false }
                    showErrorMessage(result.message)
                }
            }
        }.launchIn(viewModelScope)
    }

    fun deleteBook() {
        deleteBookJob?.cancel()
        book.value?.let { book ->
            deleteBookJob = bookUseCases.deleteBook(book.id).onEach { result ->
                when (result) {
                    is Resource.Loading -> {
                        _isLoading.update { true }
                    }

                    is Resource.Success -> {
                        _isLoading.update { false }
                        navigateBack()
                    }

                    is Resource.Error -> {
                        _isLoading.update { false }
                        showErrorMessage(result.message)
                    }
                }
            }.launchIn(viewModelScope)
        }
    }


    fun updateBook() {
        updateBookJob?.cancel()
        book.value?.let { book ->
            updateBookJob = bookUseCases.updateBook(book.id, !book.checkedOut).onEach { result ->
                when (result) {
                    is Resource.Loading -> {
                        _isLoading.update { true }
                    }

                    is Resource.Success -> {
                        _isLoading.update { false }
                        navigateBack()
                    }

                    is Resource.Error -> {
                        _isLoading.update { false }
                        showErrorMessage(result.message)
                    }
                }
            }.launchIn(viewModelScope)
        }
    }

    private fun showErrorMessage(message: String?) = viewModelScope.launch {
        bookDetailsEventsChannel.send(BookDetailsEvent.ShowMessage(message))
    }

    private fun navigateBack() = viewModelScope.launch {
        bookDetailsEventsChannel.send(BookDetailsEvent.NavigateBack)
    }
}