package com.example.mybooks.presentation.book_details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResult
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.example.mybooks.databinding.FragmentBookDetailsBinding
import com.example.mybooks.presentation.home.BOOK_REFRESH_REQUEST
import com.example.mybooks.presentation.home.BOOK_REFRESH_RESULT
import com.example.mybooks.presentation.util.showSnackbar
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

const val ARG_BOOK_ID = "bookId"

@AndroidEntryPoint
class BookDetailsFragment : Fragment() {

    private lateinit var binding: FragmentBookDetailsBinding
    private val bookDetailsViewModel: BookDetailsViewModel by viewModels()

    private lateinit var navController: NavController

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentBookDetailsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = findNavController()

        observeBook()
        observeLoading()
        handleEvents()

        binding.apply {
            checkInCheckOutButton.setOnClickListener {
                bookDetailsViewModel.updateBook()
            }
            deleteBookButton.setOnClickListener {
                bookDetailsViewModel.deleteBook()
            }
        }
    }

    private fun observeBook() {
        bookDetailsViewModel.book.observe(viewLifecycleOwner) {
            it.let { book ->
                binding.item = it
            }
        }
    }

    private fun observeLoading() {
        bookDetailsViewModel.isLoading.observe(viewLifecycleOwner) { isLoading ->
            binding.loading.visibility = if (isLoading) View.VISIBLE else View.GONE
        }
    }

    private fun handleEvents() {
        viewLifecycleOwner.lifecycleScope.launch {
            bookDetailsViewModel.bookDetailEvent.collect { event ->
                when (event) {
                    is BookDetailsEvent.ShowMessage -> {
                        showSnackbar(event.message)
                    }

                    is BookDetailsEvent.NavigateBack -> {
                        setFragmentResult(
                            BOOK_REFRESH_REQUEST,
                            bundleOf(BOOK_REFRESH_RESULT to true)
                        )
                        navController.popBackStack()
                    }
                }
            }
        }
    }
}