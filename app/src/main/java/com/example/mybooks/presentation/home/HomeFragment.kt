package com.example.mybooks.presentation.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResultListener
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.mybooks.R
import com.example.mybooks.databinding.FragmentHomeBinding
import com.example.mybooks.presentation.book_details.ARG_BOOK_ID
import com.example.mybooks.presentation.util.onQueryTextChanged
import com.example.mybooks.presentation.util.showSnackbar
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch


const val BOOK_REFRESH_REQUEST = "book_detail_request"
const val BOOK_REFRESH_RESULT = "book_detail_result"

@AndroidEntryPoint
class HomeFragment : Fragment(), BooksAdapter.BooksClickListener {

    private val homeViewModel: HomeViewModel by viewModels()
    private lateinit var binding: FragmentHomeBinding
    private val booksAdapter by lazy { BooksAdapter(this) }

    private lateinit var navController: NavController

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = findNavController()

        setupActionBar()
        setupRecyclerView()
        setupRefreshLayout()
        observeBooks()
        observeLoading()
        handleEvents()
        getFragmentResult()

        binding.fabAddBook.setOnClickListener {
            navController.navigate(R.id.action_homeFragment_to_addBookFragment)
        }
    }

    private fun getFragmentResult() {
        setFragmentResultListener(BOOK_REFRESH_REQUEST) { _, bundle ->
            val result = bundle.getBoolean(BOOK_REFRESH_RESULT, false)
            if (result) {
                homeViewModel.getBooks()
            }
        }
    }

    private fun setupActionBar() {
        (requireActivity() as AppCompatActivity).supportActionBar?.run {
            setDisplayHomeAsUpEnabled(false)
            show()
        }
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_books, menu)
        val searchItem = menu.findItem(R.id.action_search)
        val searchView = searchItem?.actionView as SearchView
        searchView.onQueryTextChanged(homeViewModel::onSearchTextChange)
    }

    private fun setupRecyclerView() {
        binding.recyclerViewBooks.apply {
            adapter = booksAdapter
            layoutManager = LinearLayoutManager(requireContext())
            setHasFixedSize(true)
        }
    }

    private fun setupRefreshLayout() {
        binding.refresh.setOnRefreshListener {
            homeViewModel.getBooks()
            binding.refresh.isRefreshing = false
        }
    }

    private fun observeBooks() {
        homeViewModel.books.observe(viewLifecycleOwner, booksAdapter::submitList)
    }

    private fun observeLoading() {
        homeViewModel.isLoading.observe(viewLifecycleOwner) { isLoading ->
            binding.loading.visibility = if (isLoading) View.VISIBLE else View.GONE
            binding.fabAddBook.visibility = if (!isLoading) View.VISIBLE else View.GONE
        }
    }

    private fun handleEvents() {
        viewLifecycleOwner.lifecycleScope.launch {
            homeViewModel.homeEvent.collect { event ->
                when (event) {
                    is HomeEvent.ShowMessage -> {
                        showSnackbar(event.message)
                    }
                }
            }
        }
    }

    override fun onItemClick(id: String) {
        navigateToDetails(id)
    }

    override fun onDeleteClick(id: String) {
        homeViewModel.deleteBook(id)
    }

    override fun onCheckInCheckOutClick(id: String, checkOut: Boolean) {
        homeViewModel.updateBook(id, checkOut)
    }

    private fun navigateToDetails(id: String) {
        val bundle = Bundle().apply {
            putString(ARG_BOOK_ID, id)
        }
        navController.navigate(R.id.action_homeFragment_to_bookDetailsFragment, bundle)
    }

    override fun onDestroy() {
        super.onDestroy()
        homeViewModel.books.removeObservers(this)
    }
}