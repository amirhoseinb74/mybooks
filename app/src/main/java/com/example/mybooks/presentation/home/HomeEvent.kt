package com.example.mybooks.presentation.home

sealed class HomeEvent {
    data class ShowMessage(val message: String?) : HomeEvent()
}