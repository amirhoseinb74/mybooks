package com.example.mybooks.presentation.add_book

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResult
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.example.mybooks.databinding.FragmentAddBookBinding
import com.example.mybooks.presentation.home.BOOK_REFRESH_REQUEST
import com.example.mybooks.presentation.home.BOOK_REFRESH_RESULT
import com.example.mybooks.presentation.util.showSnackbar
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@AndroidEntryPoint
class AddBookFragment : Fragment() {

    private lateinit var binding: FragmentAddBookBinding
    private val addBookViewModel: AddBookViewModel by viewModels()

    private lateinit var navController: NavController

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentAddBookBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = findNavController()

        handleSaveButton()
        observeLoading()
        handleEvents()
    }

    private fun handleSaveButton() {
        binding.apply {
            saveButton.setOnClickListener {
                addBookViewModel.addBook(
                    title = etTitle.text.toString(),
                    author = etAuthor.text.toString(),
                    genre = etGenre.text.toString(),
                    yearPublished = etYearPublished.text.toString(),
                )
            }
        }
    }

    private fun observeLoading() {
        addBookViewModel.isLoading.observe(viewLifecycleOwner) { isLoading ->
            binding.loading.visibility = if (isLoading) View.VISIBLE else View.GONE
        }
    }

    private fun handleEvents() {
        viewLifecycleOwner.lifecycleScope.launch {
            addBookViewModel.addBookEvent.collect { event ->
                when (event) {
                    is AddBookEvent.ShowMessage -> {
                        showSnackbar(event.message)
                    }

                    is AddBookEvent.NavigateBack -> {
                        setFragmentResult(
                            BOOK_REFRESH_REQUEST,
                            bundleOf(BOOK_REFRESH_RESULT to true)
                        )
                        navController.popBackStack()
                    }
                }
            }
        }
    }
}